from django.apps import AppConfig


class ProjectHolderConfig(AppConfig):
    name = 'project_holder'
