# Make Our Planet Great Again Django

## Commande pour lancer l'application

sudo docker-compose up

## Accéder au site web

http://127.0.0.1:8000/

## Pour acceder au liens lié aux porteur, financeur et evaluateur 

Evaluateur : http://localhost:8000/project-evaluator/   

Porteur : http://localhost:8000/project-holder/

Financeur : http://localhost:8000/project-funder/
