from django.shortcuts import render


def index(request):
    return render(request, 'project-evaluator/index.html')


def actionsEvaluator(request):
    return render(request, 'project-evaluator/actionsEvaluateur.html')


def updateEvalProject(request):
    return render(request, 'project-evaluator/updateEvalProject.html')


def viewEvalProject(request):
    return render(request, 'project-evaluator/viewEvalProject.html')
