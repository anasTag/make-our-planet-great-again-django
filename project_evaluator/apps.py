from django.apps import AppConfig


class ProjectEvaluatorConfig(AppConfig):
    name = 'project_evaluator'
