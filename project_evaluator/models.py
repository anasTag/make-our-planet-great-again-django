from django.db import models

from make_our_planet_great_again.models import ProjectModels
from project_holder.models import ProjectHolderModels
from make_our_planet_great_again.models import ProjectUserModels


class ProjectEvaluatorModels(ProjectUserModels):
    name = models.CharField(max_length=255)


class ProjectEvaluationProjectModels(models.Model):
    projectEvaluator = models.ForeignKey(ProjectEvaluatorModels, on_delete=models.CASCADE)
    project = models.ForeignKey(ProjectModels, on_delete=models.CASCADE)
    evaluation = models.IntegerField()


class ProjectEvaluationHolderModels(models.Model):
    projectEvaluator = models.ForeignKey(ProjectEvaluatorModels, on_delete=models.CASCADE)
    projectHolder = models.ForeignKey(ProjectHolderModels, on_delete=models.CASCADE)
    evaluation = models.IntegerField()
