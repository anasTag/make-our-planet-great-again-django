#Download base image ubuntu 20.04
FROM ubuntu:20.04


# LABEL about the image
LABEL maintainer="Théo_Mohamed_Anas"
LABEL version="1"
LABEL description="This is a docker image that contains the project Django and its dependencies"

# Update Ubuntu Software repository
RUN apt-get update -yq && apt-get upgrade -yq

# Install Python and Git and venv
RUN apt-get install python3-venv -yq
RUN apt-get install python3.8 -yq 
RUN apt-get install git -yq 
RUN apt-get install python3-pip -yq
RUN apt-get install python3-dev -yq
RUN apt-get install default-libmysqlclient-dev -yq
RUN apt-get install build-essential -yq
# Run venv
RUN python3 -m venv /opt/venv

# Clone our repository
RUN git clone https://gitlab.com/anasTag/djangowebsite.git


# Install dependencies:

#RUN . /opt/venv/bin/activate && pip install -r requirements.txt
WORKDIR djangowebsite
CMD /opt/venv/bin/activate 
RUN pip3 install -r requirements.txt 

# Run the application:
EXPOSE 8000
CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]