from django.db import models


class ProjectUserModels(models.Model):
    login = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    karmaPoints = models.IntegerField()

    class Meta:
        abstract = True


class ProjectModels(models.Model):
    name = models.CharField(max_length=255)
    idea = models.CharField(max_length=255)
    description = models.TextField()
    plan = models.TextField()
    image = models.ImageField(upload_to='images/')