from django.shortcuts import render


def index(request):
    return render(request, 'make-our-planet-great-again/index.html')


def contact(request):
    return render(request, 'make-our-planet-great-again/contact.html')


def register(request):
    return render(request, 'make-our-planet-great-again/register.html')


def login(request):
    return render(request, 'make-our-planet-great-again/login.html')


def mention_legales(request):
    return render(request, 'make-our-planet-great-again/mention_legales.html')


def forgot_password(request):
    return render(request, 'make-our-planet-great-again/forgot_password.html')
