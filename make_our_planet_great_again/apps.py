from django.apps import AppConfig


class ProjectMopgaConfig(AppConfig):
    name = 'make_our_planet_great_again'
