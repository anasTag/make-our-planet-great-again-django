from django.shortcuts import render


def index(request):
    return render(request, 'project-funder/indexf.html')


def actionsFunder(request):
    return render(request, 'project-funder/actionsFinanceur.html')


def updateFinProject(request):
    return render(request, 'project-funder/updateFinProject.html')


def viewFinProject(request):
    return render(request, 'project-funder/viewFinProject.html')

