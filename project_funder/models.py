from django.db import models

from make_our_planet_great_again.models import ProjectModels
from make_our_planet_great_again.models import ProjectUserModels


class ProjectFunderModels(ProjectUserModels):
    name = models.CharField(max_length=255)


class ProjectFundingsModels(models.Model):
    projectFunder = models.ForeignKey(ProjectFunderModels, on_delete=models.CASCADE)
    project = models.ForeignKey(ProjectModels, on_delete=models.CASCADE)
    budget = models.FloatField()

